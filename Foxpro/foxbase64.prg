#DEFINE CRYPT_STRING_BASE64 0x0001
#DEFINE CRYPT_STRING_NOCRLF 0x40000000
#DEFINE CRYPT_STRING_NOCR 0x80000000

CLEAR 
DO declare
 
LOCAL cBuffer, cEncoded, cDecoded
 
TEXT TO cBuffer NOSHOW
親●曾芊曾芊森艘佪●佪●親●親� 芊艘芊� � ●�
﹌艘親●佪● 芊森●芊 插侅芊艘 tions. All communication 
with a cryptographic service provider (CSP) occurs through 
these functions.
 
A CSP is an independent module that performs all cryptographic 
operations. At least one CSP is required with each application 
that uses cryptographic functions. A single application can 
occasionally use more than one CSP.
ENDTEXT
 
*cEncoded = ToBase64(m.cBuffer)
*? cEncoded
 
*cDecoded = FromBase64(cEncoded)
*? cDecoded
 
 
 ? FromBase64([MjAxNDYxNzE3MzM=])
* end of main
 
FUNCTION ToBase64(cSrc)
    LOCAL nFlags, nBufsize, cDst
    nFlags=CRYPT_STRING_BASE64
 
    nBufsize=0
    = CryptBinaryToString(@cSrc, LEN(cSrc),;
        m.nFlags, NULL, @nBufsize)
 
    cDst = REPLICATE(CHR(0), m.nBufsize)
    IF CryptBinaryToString(@cSrc, LEN(cSrc), m.nFlags,;
        @cDst, @nBufsize) = 0
        RETURN ""
    ENDIF
RETURN cDst
 
FUNCTION FromBase64(cSrc)
    LOCAL nFlags, nBufsize, cDst
    nFlags=CRYPT_STRING_BASE64
 
    nBufsize=0
    = CryptStringToBinary(@cSrc, LEN(m.cSrc),;
        nFlags, NULL, @nBufsize, 0,0)
 
    cDst = REPLICATE(CHR(0), m.nBufsize)
    IF CryptStringToBinary(@cSrc, LEN(m.cSrc),;
        nFlags, @cDst, @nBufsize, 0,0) = 0
        RETURN ""
    ENDIF
RETURN m.cDst
 
PROCEDURE declare
    DECLARE INTEGER CryptBinaryToString IN Crypt32;
        STRING @pbBinary, LONG cbBinary, LONG dwFlags,;
        STRING @pszString, LONG @pcchString
 
    DECLARE INTEGER CryptStringToBinary IN crypt32;
        STRING @pszString, LONG cchString, LONG dwFlags,;
        STRING @pbBinary, LONG @pcbBinary,;
        LONG pdwSkip, LONG pdwFlags