/*
Navicat MySQL Data Transfer

Source Server         : localhsot
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : taxbase

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-06-19 22:40:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for officecode
-- ----------------------------
DROP TABLE IF EXISTS `officecode`;
CREATE TABLE `officecode` (
  `officecode` varchar(8) DEFAULT NULL,
  `officenm` varchar(200) DEFAULT NULL,
  `regionid` char(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rdip
-- ----------------------------
DROP TABLE IF EXISTS `rdip`;
CREATE TABLE `rdip` (
  `office_Name` varchar(255) DEFAULT NULL,
  `IPaddress` varchar(18) DEFAULT NULL,
  `EndIPaddress` varchar(18) DEFAULT NULL,
  `getway` varchar(18) DEFAULT NULL COMMENT 'Getway'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tb_frmcd9091
-- ----------------------------
DROP TABLE IF EXISTS `tb_frmcd9091`;
CREATE TABLE `tb_frmcd9091` (
  `UID` varchar(25) DEFAULT NULL COMMENT 'เลขระบุเอกสาร',
  `DLN` varchar(42) DEFAULT NULL COMMENT 'เลขคุมเอกสาร',
  `TIN` varchar(10) DEFAULT NULL COMMENT 'เลขประจำตัวผู้เสียภาษี',
  `RCTP` varchar(2) DEFAULT NULL,
  `BRN` varchar(4) DEFAULT NULL COMMENT 'สาขา',
  `RCNR` varchar(11) DEFAULT NULL,
  `RCDT` varchar(8) DEFAULT NULL,
  `TXTP` varchar(3) DEFAULT NULL COMMENT 'ประเภทภาษี',
  `FMCD` varchar(7) DEFAULT NULL COMMENT 'รหัสแบบ',
  `FMPI` varchar(1) DEFAULT NULL COMMENT 'ประเภทกำหนดการยื่นแบบ',
  `FMPF` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบจาก',
  `FMPT` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบถึง',
  `PMAM` decimal(18,2) DEFAULT NULL COMMENT 'เบี้ยปรับ',
  `RCOF` decimal(8,0) DEFAULT NULL COMMENT 'สำนักงานที่รับแบบ',
  `HOF` varchar(8) DEFAULT NULL COMMENT 'รหัสสำนักงานภูมิลำเนาผู้เสียภาษี',
  `PROC_TIME` varchar(40) DEFAULT NULL,
  `PIN` varchar(13) DEFAULT NULL COMMENT 'เลขบัตรประชาชน',
  `NAME` varchar(255) DEFAULT NULL COMMENT 'ชื่อ สกุล ผู้เสียภาษี',
  `TINPIN` varchar(23) DEFAULT NULL COMMENT 'เชื่อมข้อมูลโดย concat'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tb_tclfrmrdtinpin0
-- ----------------------------
DROP TABLE IF EXISTS `tb_tclfrmrdtinpin0`;
CREATE TABLE `tb_tclfrmrdtinpin0` (
  `UID` varchar(25) DEFAULT NULL COMMENT 'เลขระบุเอกสาร',
  `DLN` varchar(42) DEFAULT NULL COMMENT 'เลขคุมเอกสาร',
  `TIN` varchar(10) DEFAULT NULL COMMENT 'เลขประจำตัวผู้เสียภาษี',
  `RCTP` varchar(2) DEFAULT NULL,
  `BRN` varchar(4) DEFAULT NULL COMMENT 'สาขา',
  `RCNR` varchar(11) DEFAULT NULL,
  `RCDT` varchar(8) DEFAULT NULL,
  `TXTP` varchar(3) DEFAULT NULL COMMENT 'ประเภทภาษี',
  `FMCD` varchar(7) DEFAULT NULL COMMENT 'รหัสแบบ',
  `FMPI` varchar(1) DEFAULT NULL COMMENT 'ประเภทกำหนดการยื่นแบบ',
  `FMPF` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบจาก',
  `FMPT` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบถึง',
  `PMAM` decimal(18,2) DEFAULT NULL COMMENT 'เบี้ยปรับ',
  `RCOF` decimal(8,0) DEFAULT NULL COMMENT 'สำนักงานที่รับแบบ',
  `HOF` varchar(8) DEFAULT NULL COMMENT 'รหัสสำนักงานภูมิลำเนาผู้เสียภาษี',
  `PROC_TIME` varchar(40) DEFAULT NULL,
  `PIN` varchar(13) DEFAULT NULL COMMENT 'เลขบัตรประชาชน',
  `NAME` varchar(255) DEFAULT NULL COMMENT 'ชื่อ สกุล ผู้เสียภาษี',
  `TINPIN` varchar(23) DEFAULT NULL COMMENT 'เชื่อมข้อมูลโดย concat'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tb_tinpin0
-- ----------------------------
DROP TABLE IF EXISTS `tb_tinpin0`;
CREATE TABLE `tb_tinpin0` (
  `UID` varchar(25) DEFAULT NULL COMMENT 'เลขระบุเอกสาร',
  `DLN` varchar(42) DEFAULT NULL COMMENT 'เลขคุมเอกสาร',
  `TIN` varchar(10) DEFAULT NULL COMMENT 'เลขประจำตัวผู้เสียภาษี',
  `RCTP` varchar(2) DEFAULT NULL,
  `BRN` varchar(4) DEFAULT NULL COMMENT 'สาขา',
  `RCNR` varchar(11) DEFAULT NULL,
  `RCDT` varchar(8) DEFAULT NULL,
  `TXTP` varchar(3) DEFAULT NULL COMMENT 'ประเภทภาษี',
  `FMCD` varchar(7) DEFAULT NULL COMMENT 'รหัสแบบ',
  `FMPI` varchar(1) DEFAULT NULL COMMENT 'ประเภทกำหนดการยื่นแบบ',
  `FMPF` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบจาก',
  `FMPT` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบถึง',
  `PMAM` decimal(18,2) DEFAULT NULL COMMENT 'เบี้ยปรับ',
  `RCOF` decimal(8,0) DEFAULT NULL COMMENT 'สำนักงานที่รับแบบ',
  `HOF` varchar(8) DEFAULT NULL COMMENT 'รหัสสำนักงานภูมิลำเนาผู้เสียภาษี',
  `PROC_TIME` varchar(40) DEFAULT NULL,
  `PIN` varchar(13) DEFAULT NULL COMMENT 'เลขบัตรประชาชน',
  `NAME` varchar(255) DEFAULT NULL COMMENT 'ชื่อ สกุล ผู้เสียภาษี',
  `TINPIN` varchar(23) DEFAULT NULL COMMENT 'เชื่อมข้อมูลโดย concat'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tclfrmrd
-- ----------------------------
DROP TABLE IF EXISTS `tclfrmrd`;
CREATE TABLE `tclfrmrd` (
  `UID` varchar(25) DEFAULT NULL COMMENT 'เลขระบุเอกสาร',
  `DLN` varchar(42) DEFAULT NULL COMMENT 'เลขคุมเอกสาร',
  `TIN` varchar(10) DEFAULT NULL COMMENT 'เลขประจำตัวผู้เสียภาษี',
  `RCTP` varchar(2) DEFAULT NULL,
  `BRN` varchar(4) DEFAULT NULL COMMENT 'สาขา',
  `RCNR` varchar(11) DEFAULT NULL,
  `RCDT` varchar(8) DEFAULT NULL,
  `TXTP` varchar(3) DEFAULT NULL COMMENT 'ประเภทภาษี',
  `FMCD` varchar(7) DEFAULT NULL COMMENT 'รหัสแบบ',
  `FMPI` varchar(1) DEFAULT NULL COMMENT 'ประเภทกำหนดการยื่นแบบ',
  `FMPF` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบจาก',
  `FMPT` varchar(8) DEFAULT NULL COMMENT 'รอบระยะบัญชีที่ยื่นแบบถึง',
  `PMAM` decimal(18,2) DEFAULT NULL COMMENT 'เบี้ยปรับ',
  `RCOF` decimal(8,0) DEFAULT NULL COMMENT 'สำนักงานที่รับแบบ',
  `HOF` varchar(8) DEFAULT NULL COMMENT 'รหัสสำนักงานภูมิลำเนาผู้เสียภาษี',
  `PROC_TIME` varchar(40) DEFAULT NULL,
  `PIN` varchar(13) DEFAULT NULL COMMENT 'เลขบัตรประชาชน',
  `NAME` varchar(255) DEFAULT NULL COMMENT 'ชื่อ สกุล ผู้เสียภาษี',
  `TINPIN` varchar(23) DEFAULT NULL COMMENT 'เชื่อมข้อมูลโดย concat'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tmp_pin
-- ----------------------------
DROP TABLE IF EXISTS `tmp_pin`;
CREATE TABLE `tmp_pin` (
  `pin` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- View structure for frmcd9091
-- ----------------------------
DROP VIEW IF EXISTS `frmcd9091`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `frmcd9091` AS SELECT * FROM tclfrmrd WHERE	tclfrmrd.FMCD LIKE '%90%' OR tclfrmrd.FMCD LIKE '%91%' ;

-- ----------------------------
-- View structure for tinpin0
-- ----------------------------
DROP VIEW IF EXISTS `tinpin0`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `tinpin0` AS SELECT * FROM	frmcd9091 WHERE tinpin LIKE '0             %' ;
